package servlet.test;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

import org.apache.http.client.ClientProtocolException;

public class App {

//	static final String xmlPath1 = "xml/01_Transaction_OdL.TXT";
//	static final String xmlPath2 = "xml/02_Transaction_clienti.txt";
//	static final String anagPath ="data/ID.txt";
//	static final Integer numThread = 1;
//	static final int startList = 50;
	
	static final String idpdaString = "idpda=\"1\"";
	static final String idDeviceString = "deviceid=\"359427052542349\"";
	static final String passwdString = "passwd=\"359427052542349\""; 
	static final String userString = "user=\"359427052542349\"";
		
    public static void main(String[] args) throws ClientProtocolException, IOException {
    	
//Acquisizione parametri -- Eccezioni non gestite
    	
    	String url = args[0];
    	String xmlPath1 = args[1];
    	String xmlPath2 = args[2];
    	String anagPath = args[3];
    	Integer numThread = Integer.parseInt(args[4]);//numThread maggiore di lines
    	Integer startList = Integer.parseInt(args[5]);
    	Integer delay = Integer.parseInt(args[6]);
    	
//Acquisizione parametri -- Eccezioni non gestite
    	
    	String xml = loadXml(xmlPath1);
    	String xml2 = loadXml(xmlPath2);
    	
    	List<String> lines = Files.lines(Paths.get(anagPath))
    									.collect(Collectors.toList());
    	
    	if(lines.size() < numThread) {
    		System.out.println("ERRORE: il numero di Thread è maggiore del numero di ID");
    		return;
    	}
    	
    	List<Tablet> listTablet = new ArrayList<>();
    	
    	lines.stream().forEach(s -> {
    		Tablet t = new Tablet(s.split(";")[0], s.split(";")[1]);
    		listTablet.add(t);
    	});
    	
    	Vector<Tablet> list = new Vector<Tablet>();
    	
    	int j = 0;
    	for(int i = startList; i< (startList+numThread); i++) {
    		if(i < lines.size()) {
    			list.add(listTablet.get(i));
    		}else {
    			list.add(listTablet.get(j));
    			j++;
    		}
    		
    	}
 
    	ExecutorService executor = Executors.newFixedThreadPool(numThread);
    	
		String output = new StringBuilder()
				   .append("Data")
				   .append(SendThread.SEPARATOR)
				   .append("Ora")
				   .append(SendThread.SEPARATOR)
				   .append("Num_Thread")
				   .append(SendThread.SEPARATOR)
				   .append("idTab")
				   .append(SendThread.SEPARATOR)
				   .append("ODL:dur")
				   .append(SendThread.SEPARATOR)
				   .append("ODL:code")
				   .append(SendThread.SEPARATOR)
				   .append("ODL:lengthCode")
				   .append(SendThread.SEPARATOR)
				   .append("CL:dur")
				   .append(SendThread.SEPARATOR)
				   .append("CL:code")
				   .append(SendThread.SEPARATOR)
				   .append("CL:lengthCode")
				   .append(SendThread.SEPARATOR)
				   .append("delay")
				   .toString();
		
		System.out.println(output);
		
    	int k = 1;
    	
    	for(Tablet a : list) {
            
    		String idDevice = new String(a.getDeviceId());
    		String idpda = new String(a.getIdpda());
			String x1 = new String(xml);
			String x2 = new String(xml2);
			
			x1 = replaceInXml(x1, idDevice, idpda);
			x2 = replaceInXml(x2, idDevice, idpda);
			
    		SendPost post = new SendPost(url);
    		
    		ArrayList<XmlFile> xmlList = new ArrayList<XmlFile>();
    		XmlFile xmlFile1 = new XmlFile("ODL",x1);
    		XmlFile xmlFile2 = new XmlFile("CL",x2);
    		xmlList.add(xmlFile1);
    		xmlList.add(xmlFile2);
    		
   		    		
    		SendThread send = new SendThread(post, k, idDevice, xmlList, delay);
    		Thread t = new Thread (send);
    		executor.execute(t);
    		k++;	
    		
    		try {
    			Thread.sleep(delay);
    		} catch (InterruptedException e) {
    			e.printStackTrace();
    		}
    	}
    	
        executor.shutdown();
        while (!executor.isTerminated()) { }
    	
        System.out.println("END");
    
    }
    

	public static String loadXml (String file) throws FileNotFoundException {
	
    	InputStream in = new FileInputStream(file);
    	String result = new BufferedReader(new InputStreamReader(in)).lines().collect(Collectors.joining("\n"));
	
    	return result;
	
    }
    
    public static String replaceInXml (String xml, String deviceId, String idpda) { 
    	
    	xml = xml.replaceAll(idpdaString, "idpda=\"" + idpda + "\"");
    	xml = xml.replaceAll(idDeviceString, "deviceid=\"" + deviceId + "\"");
    	xml = xml.replaceAll(passwdString, "passwd=\"" + deviceId + "\"");
    	xml = xml.replaceAll(userString, "user=\"" + deviceId + "\"");
    	
    	return xml;
 
    }
    
    public static Integer random(Integer seconds) {
    	
    	double random = Math.random();
    	
    	if (random > 0.65) {
    		random = random*seconds;
    	}else {
    		random = -random*seconds;
    	}
    	
    	return (int)random;
    }
    
}
