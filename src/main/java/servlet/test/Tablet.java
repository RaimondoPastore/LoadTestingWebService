package servlet.test;

public class Tablet {
	
	private String idpda;
	private String deviceId;

	
	public String getIdpda() {
		return idpda;
	}


	public void setIdpda(String idpda) {
		this.idpda = idpda;
	}


	public String getDeviceId() {
		return deviceId;
	}


	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}


	public Tablet(String idpda, String deviceId) {
		
		this.idpda = idpda;
		this.deviceId = deviceId;
	}

}
