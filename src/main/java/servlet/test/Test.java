package servlet.test;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.stream.Collectors;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

public class Test {
	
	static final String url = "http://10.79.202.212:8080/ergwfm/PDAServlet";
	static final String xmlPath1 = "xml/01 Transaction OdL.TXT";
	static final String xmlPath2 = "xml/02 Transaction clienti.txt";
	static final String fileOutPath1 = "testFile1.txt";
	static final String fileOutPath2 = "testFile2.txt";
	
	public static void main(String[] args) throws ClientProtocolException, IOException {
		
        FileWriter f = new FileWriter(fileOutPath2);
        BufferedWriter b = new BufferedWriter(f);
        PrintWriter p = new PrintWriter(b);
        
        String xml = loadXml(xmlPath2);
        
        HttpPost httpPost = new HttpPost(url);
        

        httpPost.setHeader("Content-Type", "application/xml");
        httpPost.setEntity(new StringEntity(xml));
        
        HttpClient client = HttpClientBuilder.create().build();
                
        HttpResponse response = client.execute(httpPost);
        
        p.println("Richiesta 'POST' a URL : " + url);
        p.println("\n");
        p.println("Response Code : " +
                       response.getStatusLine().getStatusCode());
        p.println("\n");
        
        
//***********************************************************
		
        HttpEntity entity = response.getEntity();
        String responseString = EntityUtils.toString(entity, "UTF-8");
        
        if (entity != null) {
            httpPost.abort();
            System.out.println("Abort");
        }
        
        p.println(responseString);
        p.close();
        b.close();
        f.close();
		
        
 //***********************************************************            

//		BufferedReader rd = new BufferedReader(
//                       new InputStreamReader(response.getEntity().getContent()));
//
//		StringBuffer result = new StringBuffer();
//		String line = new String();
//		while ((line = rd.readLine()) != null) {
//			result.append(line);
//		}
//
//		p.println(result.toString());
//        
//        p.close();
//        b.close();
//        f.close();
		
	}
	
	
	public HttpResponse sendPost(HttpPost httpPost, String xml) throws ClientProtocolException, IOException {
		
        httpPost.setHeader("Content-Type", "application/xml");
        httpPost.setEntity(new StringEntity(xml));
        
        HttpClient client = HttpClientBuilder.create().build();
                
        HttpResponse response = client.execute(httpPost);
        
        return response;
		
	}
        
    
	static String loadXml (String file) throws FileNotFoundException {
		
		InputStream in = new FileInputStream(file);
		String result = new BufferedReader(new InputStreamReader(in)).lines().collect(Collectors.joining("\n"));
		
		return result;
		
	}
	
	
}